const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('assets/js/control.js', 'public/js')
   .sass('assets/css/users/control.scss', 'public/css')
	.browserSync({
		proxy: 'localhost:8080',
	  	port: '5000',
	  	files: [
	  		'*',
	  		'*/*',
	  	],
	});


		