const _ = require('lodash');
const { Intersection } = require('../classes/Intersection.js');
const YELLOW_DELAY = 5000;

class Decider {
	constructor(firebase, roundTime) {
		this.firebase = firebase;
		this.roundTime = roundTime;
	}

	// decide on next green on every intersection
	async decideIntersections() {
        const traffic = await this.firebase.config('traffic');
        console.log('traffic configuration is: ' + traffic);
        if (traffic) {
            this.firebase.intersections.once('value', (data) => {
                // map through each intersection and distribute the green for each
                _.mapValues(data.val(), (intersectionData, intersectionId) => {
                    this.distributeGreen(intersectionData, intersectionId);
                });
            });
        }
	}

	distributeGreen(intersectionData, intersectionId) {
		const intersection = new Intersection(this.firebase, intersectionId, intersectionData);

        // subtract yellow-delays from greenable time of the round
        const greenableTime = this.roundTime - (intersection.directions.names.length * YELLOW_DELAY);
        
        // get sum of all num_vehicles_waiting
        const numVehiclesWaiting = _.sumBy(_.values(intersection.directions.data), 'num_vehicles');

        // compute number green-time
        let greenDistribution = _.mapValues(intersection.directions.data, (direction) => {
            return (direction.num_vehicles / numVehiclesWaiting) * greenableTime;
        });

        // distribute green time
        let runningTime = 0;
        let lastGreen;
        for (let directionName in greenDistribution) {
            console.log(directionName + ' greens after ' + runningTime);
        	setTimeout(() => {
                console.log(directionName + ' is now set to green');
        		// set to green after running all other directions have been set
        		intersection.setToGreen(directionName, YELLOW_DELAY);
        	}, runningTime);

        	// add green-time to running time for next direction
        	runningTime += greenDistribution[directionName] + YELLOW_DELAY;
            lastGreen = directionName;
        }

        setTimeout(() => {
            intersection.setToRed(lastGreen, YELLOW_DELAY);
        }, runningTime);
	}
}

exports.Decider = Decider;