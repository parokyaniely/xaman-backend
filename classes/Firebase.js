const moment = require('moment');
const admin = require('firebase-admin');
const { Direction } = require('../classes/Direction.js');
const credentials = require('../keys/sampleFirebaseProject-283eab1532d8.json');

class Firebase {
    constructor() {
        // initialize the app with a service account, granting admin privileges
        admin.initializeApp({
            credential: admin.credential.cert(credentials),
            databaseURL: 'https://samplefirebaseproject-187613.firebaseio.com'
        });
        this.db = admin.database();
        this.logs = this.db.ref('logs');
        this.buses = this.db.ref('buses');
        this.intersections = this.db.ref('intersections');
    }

    async config(property) {
        return await this.get('config/' + property);
    }

    async get(property) {
        const data = await this.db.ref(property).once('value');
        return data.val();
    }

    /**
     * Syncs a direction to Firebase
     *
     * @param  {[type]} req
     * @param  {[type]} response
     * @return {[type]}
     */
    syncDirection(req, response) {
        // get post body
        const intersectionId = req.body.intersection_id;
        const directionName = req.body.direction_name;

        // log the request and result
        this.logs.push({
            ...req.body,
            ...response,
        });

        // create direction object
        const direction = new Direction(this.db, intersectionId, directionName);

        // set initial value of called
        direction.setInitialValue('called', 0);
        direction.setInitialValue('light', 'red');

        // set values from request
        direction.set('num_vehicles', response.num_detected);
        direction.set('last_updated', moment().format());
        direction.set('last_image', req.file.cloudStoragePublicUrl)
    }
}

exports.Firebase = Firebase;
