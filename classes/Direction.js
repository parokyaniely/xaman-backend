class Direction {
    constructor(firebaseDatabase, intersectionId, directionName) {
        this.db = firebaseDatabase;
        this.intersectionId = intersectionId;
        this.directionName = directionName;
        this.directionPath = 'intersections/' + this.intersectionId + '/directions/' + this.directionName;
    }

    set(property, value) {
        return this.db.ref(this.directionPath + '/' + property).set(value);
    }

    async setInitialValue(property, initialValue) {
        const value = await this.get(property);
        if (value === null) {
            this.set(property, initialValue);
        }
    }

    async get(property) {
        const data = await this.db.ref(this.directionPath + '/' + property).once('value');
        return data.val();
    }
}

exports.Direction = Direction;