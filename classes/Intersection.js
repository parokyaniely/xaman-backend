const _ = require('lodash');
const { Direction } = require('./Direction');
const ACCESS_TOKEN = require('../keys/particle').token;
const axios = require('axios');

class Intersection {
    constructor(firebase, intersectionId, intersectionData, yellowDelay) {
        this.firebase = firebase;
        this.db = firebase.db;
        this.intersectionId = intersectionId;
        const directionList = intersectionData.directions;
        this.directions = {
            data: directionList,
            names: Object.keys(directionList),
            firebase: _.mapValues(directionList, (direction, directionName) => {
                return new Direction(this.db, intersectionId, directionName);
            }),
        };
        this.data = intersectionData;
    }

    setToGreen(greenDirectionName, yellowDelay) {
        // set other directions to red
        _.mapValues(this.directions.data, async (direction, directionName) => {
            // if currently green and turning to red, set it to yellow for 5 seconds
            if (directionName !== greenDirectionName) {
                const light = await this.directions.firebase[directionName].get('light');
                if (light === 'green') {
                    this.setToRed(directionName, yellowDelay);
                } else if (light === 'red') {
                    // do nothing
                }
            }
        });

        // set to green after the other street's yellow delay is finished
        setTimeout(() => {
            this.setLight(greenDirectionName, 'green');
        }, yellowDelay);
    }

    setToRed(directionName, yellowDelay) {
        // set to yellow
        this.setLight(directionName, 'yellow');
        setTimeout(() => {
            this.setLight(directionName, 'red');
        }, yellowDelay);
    }

    async setLight(directionName, color) {
        this.directions.firebase[directionName].set('light', color);
        const deviceId = this.data.device_id;
        const lightId = this.directions.data[directionName].light_id;

        // check if direction has a device_id set, then call the device
        if (deviceId && lightId) {
            const light = await this.firebase.config('light');
            if (light) {
                axios.post('https://api.particle.io/v1/devices/' + deviceId + '/' + lightId + '?access_token=' + ACCESS_TOKEN, {
                    arg: color,
                }).catch(error => {
                    console.log('Device connection unsuccessful');
                });
            }
        }
    }
}

exports.Intersection = Intersection;