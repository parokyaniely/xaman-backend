class Bus {
	constructor(firebase, busId) {
		this.firebase = firebase;
		this.busId = busId;
        this.ref = firebase.buses.child(busId);
	}

	// decide on next green on every intersection
	async exists() {
        const data = await this.get();
        if (value) {
            return true;
        }

        return false;
	}

    async get() {
        const data = await this.ref.once('value');
        return data.val();
    }

    async marks() {
        const data = await this.get();
        if (data && data.marks) {
            return data.marks;
        }

        return 0;
    }

    async mark() {
        let marks = await this.marks();
        this.set('marks', ++marks);
    }

    async unmark() {
        let marks = await this.marks();
        this.set('marks', --marks);
    }

    async respond(res) {
        res.json({
            status: 'success',
            bus: await this.get()
        });
    }

    set(property, value) {
        return this.ref.child(property).set(value);
    }
}

exports.Bus = Bus;