const axios = require('axios');

const SIGHT_HOUND_KEY = require('../keys/sighthound.json').key;

async function detect(req, res = null) {
    const objectUrlMap = {
        vehicle: 'https://dev.sighthoundapi.com/v1/recognition?objectType=vehicle',
        person: 'https://dev.sighthoundapi.com/v1/detections?type=person',
    };

    const detection = await axios.post(objectUrlMap[req.params.object], {
        image: req.file.cloudStoragePublicUrl,
    }, {
        headers: {
            'X-Access-Token': SIGHT_HOUND_KEY,
            'Content-Type': 'application/json',
        }
    });

    // construct the response
    const response = {
        status: 'success',
        num_detected: detection.data.objects.length,
        image: req.file.cloudStoragePublicUrl,
        detection_data: detection.data,
    };

    if (res) {
        res.json(response);
    }

    return response;
}

module.exports = {
    detect,
};