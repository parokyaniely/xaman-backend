const { validationResult } = require('express-validator/check');

function validate(req, res, next) {
    try {
        validationResult(req).throw();

        next();
    } catch (err) {
        res.status(422).json({
            status: 'error',
            errors: err.mapped(),
        });
    }
}

module.exports = {
    validate,
};