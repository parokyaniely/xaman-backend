// Copyright 2017, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

'use strict';

const mime = require('mime');
const moment = require('moment');
const storage = require('@google-cloud/storage');
const googleCloudStorage = storage({
    keyFilename: 'keys/JCTrial-2cada56401f7.json'
});
const CLOUD_BUCKET = 'govhack';
const bucket = googleCloudStorage.bucket(CLOUD_BUCKET);

// Returns the public, anonymously accessable URL to a given Cloud Storage
// object.
// The object's ACL has to be set to public read.
function getPublicUrl(filename) {
  return `https://storage.googleapis.com/${CLOUD_BUCKET}/${filename}`;
}

function direction(req, res, next) {
    if (! req.file) {
        return res.status(400).send("No file uploaded.");
    }

    upload(req, next, 'intersection_' + req.body.intersection_id
        + '/' + 'direction_' + req.body.direction_name
        + '/' + filename(req));
}

function detection(req, res, next) {
    if (! req.file) {
        return res.status(400).send("No file uploaded.");
    }

    // upload
    upload(req, next, 'detections/' + req.params.object + '/' + filename(req));
}

function filename(req) {
    return moment().format('YYYY_MM_DD_HH_mm_ss') + '.' + mime.getExtension(req.file.mimetype);
}

function people(req, res, next) {
    if (! req.file) {
        return res.status(400).send("No file uploaded.");
    }

    upload(req, next, 'people/' + req.params.place + '/' + req.body.id + '/' + filename(req));
}

function upload(req, next, gcsname) {
    const file = bucket.file(gcsname);

    const stream = file.createWriteStream({
        metadata: {
            contentType: req.file.mimetype
        }
    });

    stream.on('error', (err) => {
        req.file.cloudStorageError = err;
        console.log(err);
        next(err);
    });

    stream.on('finish', () => {
        req.file.cloudStorageObject = gcsname;
        file.makePublic().then(() => {
            req.file.cloudStoragePublicUrl = getPublicUrl(gcsname);
            next();
        });
    });

    stream.end(req.file.buffer);
}

// Multer handles parsing multipart/form-data requests.
// This instance is configured to store images in memory.
// This makes it straightforward to upload to Cloud Storage.
// [START multer]
const Multer = require('multer');
const multer = Multer({
  storage: Multer.MemoryStorage,
  limits: {
    fileSize: 5 * 1024 * 1024 // no larger than 5mb
  }
});
// [END multer]

module.exports = {
  getPublicUrl,
  upload,
  direction,
  detection,
  people,
  multer
};