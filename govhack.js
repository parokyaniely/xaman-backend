const express = require('express');
const { body, param } = require('express-validator/check');

const uploader = require('./lib/uploader');
const validator = require('./lib/validator');
const detector = require('./lib/detector');

const { Firebase } = require('./classes/Firebase');
const { Decider } = require('./classes/Decider');
const { Bus } = require('./classes/Bus');

const firebase = new Firebase;

// Instantiate an express server
const app = express();

app.use(express.static('public'));

app.get('/control', (req, res) => {
    res.sendFile(__dirname + '/control.html');
});

app.post('/traffic',

    // obtain the uploaded image from the request
    uploader.multer.single('image'),

    // validate request body
    [
        body('intersection_id', 'intersection_id must be provided, alphanumeric, and first character must not be a number')
            .trim()
            .isLength({ min: 1 })
            .isAlphanumeric()
            .matches(/^[a-zA-Z].*/),
        body('direction_name', 'intersection_id must be provided, alphanumeric, and first character must not be a number')
            .trim()
            .isLength({ min: 1 })
            .isAlphanumeric()
            .matches(/^[a-zA-Z].*/),
    ],

    // return an error if validation fails
    validator.validate,

    // upload the file to GCS
    uploader.direction,

    // here we do what we need to do
    async (req, res, next) => {
        req.params.object = 'vehicle';

        // detect, respond, and get response for syncing with firebase
        const response = await detector.detect(req, res);

        // sync to firebase
        firebase.syncDirection(req, response);  
    },
);

app.post('/detect/:object',

    // obtain the uploaded image from the request
    uploader.multer.single('image'),

    // validate request parameters
    [
        param('object', 'detection type must be either "person" or "vehicle"').isIn(['person', 'vehicle']),
    ],

    // return an error if validation fails
    validator.validate,

    // upload the file to GCS
    uploader.detection,

    // detect and respond
    detector.detect
);

/**
 * Set number of people in a particular place
 */
app.post('/people/in/:place',

    // obtain the uploaded image from the request
    uploader.multer.single('image'),

    // validate request parameters
    [
        param('place', 'Place must either be "bus" or "station"').isIn(['bus', 'station']),
        body('id', 'Please provide the id of the place')
            .trim()
            .isLength({ min: 1 })
            .isAlphanumeric()
            .matches(/^[a-zA-Z].*/),
    ],

    // return an error if validation fails
    validator.validate,

    // upload the file to GCS
    uploader.people,

    // detect people in buses and sync with firebase
    async (req, res, next) => {
        req.params.object = 'person';

        // detect, respond, and get response for syncing with firebase
        const response = await detector.detect(req, res);

        // sync to firebase
        if (req.params.place === 'bus') {
            const bus = new Bus(firebase, req.body.id);
            bus.set('people', response.num_detected);
        }
    },
)

app.put('/bus/:id',

    // parse post-body
    express.json(),

    express.urlencoded({ extended: true }),

    // validate request parameters
    [
        body('latlong', 'Please provide a valid coordinate')
            .isLatLong(),
    ],

    // return an error if validation fails
    validator.validate,

    // do what we need to do
    async (req, res) => {
        // construct bus
        const bus = new Bus(firebase, req.params.id);

        // perform action
        await bus.set('latlong', req.body.latlong)

        // respond
        bus.respond(res);
    }
)

app.post('/bus/:action',

    // parse post-body
    express.json(),

    express.urlencoded({ extended: true }),

    // validate request parameters
    [
        param('action', 'action must be either "mark" or "unmark"').trim().isIn(['mark', 'unmark']),
        body('id', 'Please provide a bus id')
            .trim()
            .isLength({ min: 1 })
            .isAlphanumeric()
            .matches(/^[a-zA-Z].*/),
    ],

    // return an error if validation fails
    validator.validate,

    // do what we need to do
    async (req, res) => {
        // construct bus
        const bus = new Bus(firebase, req.body.id);

        // perform action
        await bus[req.params.action]()

        // respond
        bus.respond(res);
    }
)

app.get('/bus/:id', (req, res) => {
    // construct bus
    const bus = new Bus(firebase, req.params.id);

    // respond
    bus.respond(res);
});

app.get('/stations', async (req, res) => {
    // eager-load
    let stations = await firebase.get('stations');
    const buses = await firebase.get('buses');

    res.json(Object.values(stations).map((station) => {
        let busesOfStation = {};
        for (let i in station.buses) {
            const busId = station.buses[i];
            busesOfStation[busId] = buses[busId];
        }
        station.buses = busesOfStation;
        return station;
    }));
});

const PORT = 8080;
const ROUND_TIME = 30000;
app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`);
    console.log("Press Ctrl+C to quit.");

    // const decider = new Decider(firebase, ROUND_TIME);
    // decider.decideIntersections();

    // setInterval(() => {
    //     // decide which light to set green based on traffic data and last calls
    //     decider.decideIntersections();
    // }, ROUND_TIME);
});